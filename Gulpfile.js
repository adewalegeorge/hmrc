var gulp = require('gulp');
var pug = require('gulp-pug');
var args = require('yargs').argv;
var gulpif = require('gulp-if');
var sass = require('gulp-ruby-sass');
var term = require( 'terminal-kit' ).terminal;
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var plumber = require('gulp-plumber');
var changed = require('gulp-changed');
var uglify = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var runTimestamp = Math.round(Date.now() / 1000);
var browserSync = require('browser-sync');
var reloadBS = browserSync.reload;
var streamBS = browserSync.stream;
var isProduction = args.env === 'production';
var log = console.log;


/*--------------------------------------------
  CONFIG
----------------------------------------------*/
var config = {
  deploy: './public',
  origin: './src',
  assets: './assets'
};

/*---------------------------------------------
  BROWSER SYNC
----------------------------------------------*/
gulp.task('browser-sync', function () {
  browserSync.init(null, {
    server: {
      baseDir: config.deploy
    },
    // Change the default weinre port
    ui: {
      weinre: {
        port: 9090
      }
    }
  });
});


/*---------------------------------------------
  PUG
----------------------------------------------*/
gulp.task('pug', function() {
  // var dataFile = config.origin + '/pug/data/cabin.json';
  return gulp.src([
    config.origin + '/pug/**/*.pug',
    '!' + config.origin + '/pug/templates/**',
    '!' + config.origin + '/pug/includes/**'
  ])
  .pipe(plumber())
  .pipe(pug({ pretty: true }))
  .pipe(gulp.dest( config.deploy ))
  .pipe(browserSync.reload({stream:true}));
});


/*---------------------------------------------
  GULP STYLE
----------------------------------------------*/
gulp.task('css', function() {
  return sass(
    config.origin + '/sass/**/*.scss', {
      sourcemap: false,
      lineNumbers: false,
      style: 'nested'
    }
  )
  .pipe(plumber())
  .pipe(autoprefixer(
    'last 2 version',
    'safari 5',
    'ie 8',
    'ie 9',
    'opera 12.1',
    'ios 6', 'android 4'
  ))
  .pipe(gulp.dest(config.deploy + '/css'))
  .pipe(browserSync.reload({stream:true}));
});


/*---------------------------------------------
  GULP IMAGES
----------------------------------------------*/
gulp.task('images', function() {
  gulp.src([config.origin + '/img/**/*'])
  .pipe(gulp.dest(config.deploy + '/img'));
});


/*---------------------------------------------
    WATCH TASK
----------------------------------------------*/
gulp.task('watch', function() {
  gulp.watch(config.origin + '/pug/*.pug', ['pug']);
  gulp.watch([
    config.origin + '/pug/templates/*.pug',
    config.origin + '/pug/includes/*.pug'
  ], ['pug']);
  gulp.watch(config.origin + '/sass/**/*.scss', ['css']);
  gulp.watch('../online-booking/*.scss', ['css']);
  gulp.watch(config.origin + '/scripts/*.js', ['script']);
});


/*---------------------------------------------
    GULP EXIT TASK(s)
----------------------------------------------*/
// Rerun the task when a file changes
gulp.task('default', ['pug', 'css', 'browser-sync', 'watch']);
gulp.task('release', ['pug', 'css', 'images']);
