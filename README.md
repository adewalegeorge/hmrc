# HMRC #

HMRC Front End Test 

[![npm](https://img.shields.io/npm/v/npm.svg)](https://www.npmjs.com/)
[![built with gulp](https://img.shields.io/badge/-gulp-eb4a4b.svg?logo=data%3Aimage%2Fpng%3Bbase64%2CiVBORw0KGgoAAAANSUhEUgAAAAYAAAAOCAMAAAA7QZ0XAAAABlBMVEUAAAD%2F%2F%2F%2Bl2Z%2FdAAAAAXRSTlMAQObYZgAAABdJREFUeAFjAAFGRjSSEQzwUgwQkjAFAAtaAD0Ls2nMAAAAAElFTkSuQmCC)](http://gulpjs.com/)
[![Sass](http://pixel-cookers.github.io/built-with-badges/sass/sass-short.png)](http://sass-lang.com/)
[![Node](http://pixel-cookers.github.io/built-with-badges/node/node-short-flat.png)](https://nodejs.org/)
[![Pug](https://img.shields.io/badge/gulp--pug-3.02-%233E302F.svg)](https://pugjs.org/)




## Get up and running ##

Pulldown the repository and copy-out the `public` directory but if you're interested in the template codes please proceed below.

I have included normalize and the HMRC nta font css to render page and fonts more accurately.

Please ensure you have NODE, NPM/YARN, GULP & SASS are all installed globally on your machine.

**Gulp**

```
npm install --global gulp-cli
```

**Sass**
Please visit [![Sass](http://pixel-cookers.github.io/built-with-badges/sass/sass-short.png)](http://sass-lang.com/) to install latest


**yarn**

If you prefer Yarn please [![click here]](https://yarnpkg.com/en/docs/install) for installation guide.

**npm**

```
npm install npm -g
```
* Run `npm install` from project root directory to fetch all dependancies


### Gulp commands ###
* `gulp release` to build project public directory.
* Runing `gulp` from your command terminal will build UI and fire-up a browser window with live-reload enables using browsersync


## Project local directory structure
* Sass files are structurally stored in `src/sass`
* Image files are structurally stored in `src/img`
* Pug template files are structurally stored in `src/pug`

### Happy days! ###
![HappyDays](http://icons.iconarchive.com/icons/designbolts/despicable-me-2/128/Minion-Dancing-icon.png)

## If this isn't very helpful please contact ##
* Developer - @adewalegeorge